<?php

namespace Drupal\button_field\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Plugin implementation of the 'image button' field formatter.
 */
#[FieldFormatter(
  id: 'button_field_image',
  label: new TranslatableMarkup('Image Button'),
  description: new TranslatableMarkup('An image button formatter.'),
  field_types: ['button_field'],
)]
class ButtonFieldImage extends ButtonFieldBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'image_path' => '',
      'alt_text' => '',
      'title_text' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   *
   * @todo Provide a file upload element.
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);
    $element['image_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Image path'),
      '#default_value' => $this->getSetting('image_path'),
      '#required' => TRUE,
    ];

    $element['alt_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Alt text'),
      '#default_value' => $this->getSetting('alt_text') ?: $this->fieldDefinition->getLabel(),
      '#required' => TRUE,
    ];

    $element['title_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title text'),
      '#default_value' => $this->getSetting('title_text'),
      '#required' => FALSE,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Image path: %path', [
      '%path' => $this->getSetting('image_path'),
    ]);
    $summary[] = $this->t('Alt text: %text', [
      '%text' => $this->getSetting('alt_text') ?: $this->fieldDefinition->getLabel(),
    ]);
    $summary[] = $this->t('Title text: %text', [
      '%text' => $this->getSetting('title_text'),
    ]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  protected function elementProperties() {
    return [
      '#type' => 'image_button',
      '#src' => $this->getSetting('image_path'),
      '#attributes' => [
        'alt' => Html::escape(($this->getSetting('alt_text') ?: $this->fieldDefinition->getLabel())),
        'title' => Html::escape($this->getSetting('title_text')),
      ],
    ];
  }

}
