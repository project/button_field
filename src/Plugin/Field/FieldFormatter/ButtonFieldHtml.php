<?php

namespace Drupal\button_field\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Plugin implementation of the 'html button' field formatter.
 */
#[FieldFormatter(
  id: 'button_field_html',
  label: new TranslatableMarkup('HTML Button'),
  description: new TranslatableMarkup('An HTML button formatter.'),
  field_types: ['button_field'],
)]
class ButtonFieldHtml extends ButtonFieldBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'text' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);
    $element['text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button Text'),
      '#default_value' => $this->getSetting('text') ?: $this->fieldDefinition->getLabel(),
      '#required' => TRUE,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Button title: %text', [
      '%text' => $this->getSetting('text') ?: $this->fieldDefinition->getLabel(),
    ]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  protected function elementProperties() {
    return [
      '#type' => 'button',
      '#value' => Html::escape($this->getSetting('text') ?: $this->fieldDefinition->getLabel()),
    ];
  }

}
