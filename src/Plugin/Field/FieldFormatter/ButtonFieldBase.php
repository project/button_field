<?php

namespace Drupal\button_field\Plugin\Field\FieldFormatter;

use Drupal\button_field\ButtonFieldAjaxTrait;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base implementation for button field formatters.
 */
abstract class ButtonFieldBase extends FormatterBase implements ContainerFactoryPluginInterface {

  use ButtonFieldAjaxTrait;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Initialization method.
   *
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition from the configuration array.
   * @param array $settings
   *   The field settings from the configuration array.
   * @param string $label
   *   The field label.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Form\FormBuilderInterface $formBuilder
   *   The form_builder service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, FormBuilderInterface $formBuilder) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->formBuilder = $formBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // Get the entity that we're rendering the field for.
    $entity = $items->getEntity();
    $entity_type = $entity->getEntityType()->id();
    $entity_id = $entity->id();

    // If any additional classes have been defined then add them to the classes
    // now.
    $class = ['button_field'];
    $additional_classes = $this->getFieldSetting('additional_classes');
    if (!empty($additional_classes)) {
      $class = array_merge($class, explode(' ', $additional_classes));
    }

    // Button elements do not obey the #description index, so if a description
    // has been set we need to build our own suffix.
    $suffix = NULL;
    $description = $this->fieldDefinition->getDescription();
    if (!empty($description)) {
      $suffix = '<div class="description">' . $description . '</div>';
    }

    $id = $this->elementId(0, $items->getLangcode());
    $element = [
      '#id' => $id,
      '#name' => $id,
      '#attributes' => ['class' => $class],
      '#description' => $this->t('this is the help text'),
      '#suffix' => $suffix,
      '#entity_type' => $entity_type,
      '#entity' => $entity,
      '#field_name' => $items->getName(),
      '#limit_validation_errors' => [],
      '#attached' => [
        // @todo Fix confirmation preventing AJAX callback.
        // 'library' => ['button_field/button_field'],
        'drupalSettings' => [
          'button_field' => [
            [
              'id' => $id,
              'entity_type' => $entity_type,
              'entity_id' => $entity_id,
              'confirmation' => $this->getFieldSetting('confirmation'),
            ],
          ],
        ],
      ],
      '#ajax' => [
        'callback' => [self::class, 'handleClick'],
      ],
    ];

    $element += $this->elementProperties();
    return $this->formBuilder->getForm(
      'Drupal\button_field\Form\DummyForm',
      $element
    );
  }

  /**
   * Retrieves the properties for the current widget's element.
   *
   * @return array
   *   Properties for the widget element.
   */
  abstract protected function elementProperties();

  /**
   * Builds the id for a button field element.
   *
   * @todo Determine if there is a better way to do this.
   */
  protected function elementId($delta, $language) {
    $parts = [
      'view',
      str_replace('_', '-', $this->fieldDefinition->getName()),
      $language,
      $delta,
      'value',
    ];

    return implode('-', $parts);
  }

}
