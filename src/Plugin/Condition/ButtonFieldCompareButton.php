<?php

namespace Drupal\button_field\Plugin\Condition;

use Drupal\Core\Field\FieldConfigInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\button_field\TypedData\Options\ButtionFieldListOptions;
use Drupal\rules\Context\ContextDefinition;
use Drupal\rules\Core\Attribute\Condition;
use Drupal\rules\Core\RulesConditionBase;

/**
 * Provides a condition to compare fields by field name.
 *
 * @Condition(
 *   id = "button_field_compare_button",
 *   label = @Translation("Compare clicked button"),
 *   category = @Translation("Button Field"),
 *   context_definitions = {
 *     "clicked_field" = @ContextDefinition("entity:field_config",
 *       label = @Translation("Clicked field"),
 *       description = @Translation("The button field that was clicked."),
 *     ),
 *     "comparison_field" = @ContextDefinition("string",
 *       label = @Translation("Comparison field name"),
 *       description = @Translation("The field to compare the clicked button to."),
 *       options_provider = "\Drupal\button_field\TypedData\Options\ButtonFieldListOptions",
 *     )
 *   }
 * )
 *
 * @internal
 */
#[Condition(
  id: 'button_field_compare_button',
  label: new TranslatableMarkup('Compare clicked button'),
  module: 'button_field',
  category: new TranslatableMarkup('Button Field'),
  context_definitions: [
    'clicked_field' => new ContextDefinition(
      'entity:field_config',
      new TranslatableMarkup('Clicked field'),
      TRUE,
      FALSE,
      new TranslatableMarkup('The button field that was clicked.'),
    ),
    'comparison_field' => new ContextDefinition(
      'string',
      new TranslatableMarkup('Comparison field name'),
      TRUE,
      FALSE,
      new TranslatableMarkup('The field to compare the clicked button to.'),
      NULL,
      [],
      FALSE,
      NULL,
      ButtionFieldListOptions::class,
    ),
  ],
)]
class ButtonFieldCompareButton extends RulesConditionBase {

  /**
   * Checks if a given field configuration matches the field name.
   *
   * @param \Drupal\field\Entity\FieldConfigInterface $clicked_field
   *   The field definition.
   * @param string $comparison_field
   *   The field name.
   *
   * @return bool
   *   TRUE if the field name matches.
   */
  protected function doEvaluate(FieldConfigInterface $clicked_field, string $comparison_field) {
    return $clicked_field->getName() === $comparison_field;
  }

}
