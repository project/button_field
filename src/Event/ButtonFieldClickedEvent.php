<?php

namespace Drupal\button_field\Event;

use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Response;

/**
 * Event that is fired when a user clicks a button field on an entity.
 *
 * @see rules_user_login()
 */
class ButtonFieldClickedEvent extends GenericEvent implements ButtonFieldEventInterface {

  const EVENT_NAME = 'button_field_clicked';

  /**
   * The response for the event.
   *
   * @var \Symfony\Component\HttpFoundation\Response|null
   */
  protected ?Response $response;

  /**
   * {@inheritdoc}
   */
  public function setResponse(Response $response): void {
    $this->response = $response;
  }

  /**
   * {@inheritdoc}
   */
  public function getResponse(): ?Response {
    return $this->response;
  }

}
