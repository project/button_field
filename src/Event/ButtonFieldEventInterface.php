<?php

namespace Drupal\button_field\Event;

use Symfony\Component\HttpFoundation\Response;

/**
 * Provides an interface for button_field events.
 */
interface ButtonFieldEventInterface {

  /**
   * Sets the AJAX or other response.
   *
   * This is used internally when the event is created.
   *
   * @param \Symfony\Component\HttpFoundation\Response $response
   *   The response.
   */
  public function setResponse(Response $response): void;

  /**
   * Gets the AJAX or other response.
   *
   * This can be used to add AJAX commands to the AJAX response in the event
   * subscriber.
   *
   * @return \Symfony\Component\HttpFoundation\Response|null
   *   The response.
   */
  public function getResponse(): ?Response;

}
