(function (Drupal, once, drupalSettings) {
  Drupal.behaviors.buttonFieldBehavior = {
    attach: function(context) {
      // Iterate over each of the button fields on the page.
      drupalSettings.button_field.forEach((data) => {
        const { id, confirmation } = data;
        const [buttonElement] = once(`buttonFieldConfirm${id}`, `#${id}`, context);

        if (buttonElement && confirmation.length > 0) {
          // Adds a handler to override AJAX clicks when confirmation is set.
          // @todo Fix overriding Drupal.ajax for confirmation.
          buttonElement.addEventListener('mousedown', (e) => {
            e.stopImmediatePropagation();
            if (confirm(confirmation)) {
              // Finds the AJAX instance.
              const ajax = Drupal.ajax.instances.find(
                (instance) => instance.selector === `#${id}`
              );
              if (ajax !== null) {
                Drupal.ajax(ajax);
              }
            }
          });
        }
      });
    },
  };
})(Drupal, once, drupalSettings);
