<?php

namespace Drupal\Tests\button_field\TestSite;

use Drupal\Component\Utility\Random;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Utility\Error;
use Drupal\TestSite\TestSetupInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;

/**
 * Setup file used for button_field Nightwatch tests.
 */
class ButtonFieldInstallTestScript implements TestSetupInterface {

  /**
   * Modules that should be enabled.
   *
   * @var string[]
   */
  protected $modules = [
    'block',
    'dblog',
    'node',
    'field',
    'field_ui',
    'typed_data',
    'button_field',
    'button_field_test',
  ];

  /**
   * The random generator.
   *
   * @var \Drupal\Component\Utility\Random|null
   */
  protected $randomGenerator = NULL;

  /**
   * {@inheritdoc}
   */
  public function setup() {
    \Drupal::service('module_installer')->install($this->modules);

    try {
      $type = $this->createContentType();
      $this->createButtonFieldStorage($type);

      Node::create([
        'type' => $type,
        'title' => 'Test node',
      ])->save();
    }
    catch (EntityStorageException $e) {
      Error::logException(\Drupal::logger('button_field'), $e);
    }

    // Flush caches.
    \Drupal::cache()->deleteAll();
  }

  /**
   * Install configuration for modules.
   *
   * @param array $values
   *   Optional values to set on the content type.
   *
   * @return string
   *   The node type identifier.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createContentType(array $values = []) {
    $id = !isset($values['type']) ? $this->randomMachineName() : $values['type'];
    $values += [
      'type' => $id,
      'name' => $id,
    ];

    $type = NodeType::create($values);
    $type->save();

    // Create Entity and View displays.
    $formDisplay = EntityFormDisplay::create([
      'targetEntityType' => 'node',
      'bundle' => $id,
      'mode' => 'default',
      'status' => TRUE,
    ]);
    $formDisplay->save();

    $viewDisplay = EntityViewDisplay::create([
      'targetEntityType' => 'node',
      'bundle' => $id,
      'mode' => 'default',
      'status' => TRUE,
    ]);
    $viewDisplay->save();

    return $id;
  }

  /**
   * Creates button field on node type.
   *
   * @param string $bundle
   *   The bundle of the node type to create the field config.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createButtonFieldStorage($bundle) {
    $storage_values = [
      'field_name' => 'field_button_field',
      'entity_type' => 'node',
      'type' => 'button_field',
      'cardinality' => 1,
      'settings' => [],
    ];

    $field_storage = FieldStorageConfig::create($storage_values);
    $field_storage->save();

    $field_values = [
      'field_name' => 'field_button_field',
      'entity_type' => 'node',
      'bundle' => $bundle,
      'label' => $this->getRandomGenerator()->name(15),
      'required' => 0,
      'default_value' => [],
    ];

    $field_config = FieldConfig::create($field_values);
    $field_config->save();

    $display_id = 'node.' . $bundle . '.default';
    $formDisplay = EntityViewDisplay::load($display_id);
    $formDisplay->setComponent($field_storage->getName(), [
      'type' => 'button_field_html',
      'weight' => -1,
      'settings' => ['text' => ''],
    ]);
    $formDisplay->save();

    $viewDisplay = EntityFormDisplay::load($display_id);
    $viewDisplay->setComponent($field_storage->getName(), [
      'type' => 'button_field_html',
      'weight' => -1,
      'settings' => ['text' => ''],
    ]);
    $viewDisplay->save();
  }

  /**
   * Generates a unique random string containing letters and numbers.
   *
   * @param int $length
   *   Length of random string to generate.
   *
   * @return string
   *   Randomly generated unique string.
   *
   * @see \Drupal\Component\Utility\Random::name()
   */
  public function randomMachineName($length = 8): string {
    return $this->getRandomGenerator()->machineName($length, TRUE);
  }

  /**
   * Gets the random generator for the utility methods.
   *
   * @return \Drupal\Component\Utility\Random
   *   The random generator.
   */
  protected function getRandomGenerator(): Random {
    if (!$this->randomGenerator instanceof Random) {
      $this->randomGenerator = new Random();
    }
    return $this->randomGenerator;
  }

}
