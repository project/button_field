module.exports = {
  '@tags': ['button_field'],
  before(browser) {
    const setupFile = `${__dirname}/../../TestSite/ButtonFieldInstallTestScript.php`;
    browser.drupalInstall({ setupFile });
  },
  after(browser) {
    browser.drupalUninstall();
  },
  'Test click button field on node view triggers button field clicked event': (browser) => {
    browser
      .drupalCreateUser({
        name: 'sam',
        password: 'sam',
        permissions: [
          'bypass node access',
          'access administration pages',
          'access site reports',
        ],
      })
      .drupalLogin({ name: 'sam', password: 'sam' })
      .drupalRelativeURL('/node/1')
      .assert.visible('.button_field')
      .click('.button_field')
      .waitForElementVisible('[data-drupal-messages]', 1000)
      .assert.containsText('[data-drupal-messages]', 'Button field clicked!')
      .drupalLogAndEnd({ onlyOnError: false });
  },
};
