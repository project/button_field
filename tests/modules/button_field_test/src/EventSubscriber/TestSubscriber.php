<?php

namespace Drupal\button_field_test\EventSubscriber;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\MessageCommand;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\button_field\Event\ButtonFieldClickedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Test subscriber.
 */
class TestSubscriber implements EventSubscriberInterface {

  use MessengerTrait;

  /**
   * Initialization method.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(MessengerInterface $messenger) {
    $this->setMessenger($messenger);
  }

  /**
   * Provides a test handling for button field cilcked.
   *
   * @param \Drupal\button_field\Event\ButtonFieldClickedEvent $event
   *   The event.
   */
  public function handleButtonClicked(ButtonFieldClickedEvent $event): void {
    $this->messenger()->addStatus('Button field clicked!');

    $response = $event->getResponse();
    if ($response instanceof AjaxResponse) {
      $response->addCommand(new MessageCommand('Button field clicked!', NULL, [
        'type' => 'status',
      ]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      ButtonFieldClickedEvent::EVENT_NAME => [
        ['handleButtonClicked', 10],
      ],
    ];
  }

}
